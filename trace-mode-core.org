# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: trace-mode-core
#+subtitle: Part of the =trace-mode= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle trace-mode-core.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-lib))
(eval-when-compile (require 'rx))
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'trace-mode-macs))
(require 'akater-misc-buffers) ; akater-misc-delete-rest-of-line
                               ; decrement-integer-at-point
(declare-function replace-region-contents "subr-x")
#+end_src

* get-symbol-function-of-funcall-at-point
** Definition
#+begin_src elisp :results none
(defun-with-optional-current-buffer trace-mode-get-symbol-function-of-funcall-at-point ()
  (save-excursion
    (beginning-of-line)
    (re-search-forward (rx (zero-or-more "| ")))
    (re-search-forward (rx (one-or-more digit) " -> "))
    (car (read buffer))))
#+end_src

* get-funcall-id-at-point
** Definition
#+begin_src elisp :results none
(defun-with-optional-current-buffer trace-mode-get-funcall-id-at-point ()
  (aprog1 (cons nil nil)
    (save-excursion
      (beginning-of-line)
      (re-search-forward (rx (group (zero-or-more "| ")
                                    (one-or-more digit))
                             " -> "))
      (setf (car it) (match-string-no-properties 1)
            (cdr it) (car (read buffer))))))
#+end_src

* goto-funcall-end
** Definition
#+begin_src elisp :results none
(defun-with-optional-current-buffer trace-mode-goto-funcall-end ()
  (interactive)
  (re-search-forward
   (rx line-start
       (literal (car (trace-mode-get-funcall-id-at-point)))
       " <- "))
  (re-search-backward (rx (one-or-more digit)) (line-beginning-position)))
#+end_src

* get-funcall-end
** Definition
#+begin_src elisp :results none
(defun-with-optional-current-buffer trace-mode-get-funcall-end ()
  (save-excursion (trace-mode-goto-funcall-end)))
#+end_src

* splice-this-funcall
** Definition
#+begin_src elisp :results none
(defun trace-mode-splice-this-funcall ()
  "Transform

1 -> (f ...)
| 2 -> (g ...)
| | 3 -> (h ...)
...

to
1 -> (g ...)
| 2 -> (h ...)
...


Return value of f is thus discarded.

Point is presumed to be at the beginning of the call, i.e. at the line with 1 ->."
  (interactive)
  (let ((beg (progn (beginning-of-line) (point))))
    (trace-mode-goto-funcall-end)
    (beginning-of-line)
    (akater-misc-delete-rest-of-line)
    (previous-logical-line)
    (until (= beg (point))
      (unless (;; in-string-p
               nth 3 (syntax-ppss))
        (delete-char 2)
        (unless (looking-at (rx digit)) (re-search-forward (rx digit)))
        (decrement-integer-at-point)
        (beginning-of-line))
      (previous-logical-line))
   (akater-misc-delete-rest-of-line)))
#+end_src
