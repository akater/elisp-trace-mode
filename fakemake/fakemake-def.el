;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'trace-mode
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("trace-mode-macs"
                       "trace-mode-core"
                       "trace-extras"
                       "trace-mode")
  site-lisp-config-prefix "50"
  license "GPL-3")
